console.log('Hello Dave!')

// Arrays
// used to store multiple related data/values in a single variable.
// It is created/declared using [] brackets also known as "Array Literals"

let hobbies = ['Play Games', 'Read Books', 'Listen to Musics'];

// Array makes it easy to manage, manipulate a set of data. Arrays have different methods/functions that alow us to manage our array.
// Methods are functions associated with an object.
// Because Arrays are actually a special type of object. 

console.log(typeof hobbies);

let grades = [75.4, 98.5, 90.12, 91.50];
const planets = ['Mercury', 'Venus', 'Mars', 'Earth'];

let arraySample = ['Saitama', 'One Punch Man', 25000, true]
console.log(arraySample);

let dailyRoutine = ['Exercise', 'Eat', 'Take a Bath', 'Bootcamp', 'Sleep'];
let capitalCity = ['Manila', 'Tokyo', 'Washington D.C.', 'Canberra']
console.log(dailyRoutine);
console.log(capitalCity);

// Each item in an array is called an element.

let username1 = 'Duelist';
let username2 = 'Controller';
let username3 = 'Sentinel';
let username4 = 'Initiator';

let guildMembers = [username1, username2, username3, username4];
console.log(guildMembers);

console.log(dailyRoutine.length);
console.log(capitalCity.length);

dailyRoutine.length = dailyRoutine.length-1;
console.log(dailyRoutine.length);
console.log(dailyRoutine);

// We cannot yet delete a specific item in our array, however in the next sessions, we will learn of an array method which will allow us to do so. We can do it now, manually, but that would require us to create our own algorithm to solve the problem.

capitalCity.length--;
console.log(capitalCity);

// Can we do the same trick on a string?
// No.
let fullName = 'David Praise';
fullName.length = fullName.length-1;
console.log(fullName);

// If we can shorten the array by updating the length property, can we also lengthen or add using the same trick?

let theBeatles = ["John","Paul","Ringo","George"];
theBeatles.length++;
console.log(theBeatles);

// Accessing the elements of an array
// Accessing array element is one of the more common task we do with an array.
// This can be done through the use of array indices.
// Array elements are ordered according to index.

console.log(capitalCity);

// we can locate elements in an array through their index. 

console.log(capitalCity[0]);

let lakersLegends = ['Kobe', 'Shaq', 'Lebron', 'Magic', 'Kareem'];
console.log(lakersLegends[1]);
console.log(lakersLegends[3]);

let currentLaker = lakersLegends[2];
console.log(currentLaker);

lakersLegends[2] = "Paul";
console.log(lakersLegends);

/*
	Mini Activity
*/

let favoriteFoods = [
	'Tonkatsu',
	'Adobo',
	'Hamburger',
	'Sinigang',
	'Pizza'
];

favoriteFoods[3] = 'Palabok';
favoriteFoods[4] = 'Lahat ng luto ng mama ko';

console.log(favoriteFoods);

// access the last number of array
console.log(favoriteFoods[favoriteFoods.length-1]);

// add items in an array

let newArr = [];
console.log(newArr);
console.log(newArr[0]);

newArr[0] = 'Cloud Strike';
console.log(newArr);
console.log(newArr[1]);

newArr[1] = 'Aerith Gainsborough';
console.log(newArr);

newArr[newArr.length-1]='Tifa Lockhart';
console.log(newArr);

newArr[newArr.length]='Barrett Wallace';
console.log(newArr);

// Looping over an array

for(let index=0; index<newArr.length; index++){
	console.log(newArr[index]);
}

let numArr = [5,12,30,46,40]

for(let index = 0; index<numArr.length; index++){
	if (numArr[index]%5 === 0){
		console.log(numArr[index] + " is divisible by 5")
	} else {
		console.log(numArr[index] + " is not divisible by 5")
	}
}

// Multidemnsional Array
// arrays that contain arrays

let chessBoard = [
    ['a1', 'b1', 'c1', 'd1', 'e1', 'f1', 'g1', 'h1'],
    ['a2', 'b2', 'c2', 'd2', 'e2', 'f2', 'g2', 'h2'],
    ['a3', 'b3', 'c3', 'd3', 'e3', 'f3', 'g3', 'h3'],
    ['a4', 'b4', 'c4', 'd4', 'e4', 'f4', 'g4', 'h4'],
    ['a5', 'b5', 'c5', 'd5', 'e5', 'f5', 'g5', 'h5'],
    ['a6', 'b6', 'c6', 'd6', 'e6', 'f6', 'g6', 'h6'],
    ['a7', 'b7', 'c7', 'd7', 'e7', 'f7', 'g7', 'h7'],
    ['a8', 'b8', 'c8', 'd8', 'e8', 'f8', 'g8', 'h8']
];

console.log(chessBoard)

console.log(chessBoard[1][5])
console.log(chessBoard[7][0])
console.log(chessBoard[5][7])

